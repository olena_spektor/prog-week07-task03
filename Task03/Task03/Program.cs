﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please type in the numbers");
            var a = int.Parse(Console.ReadLine());

            var b = int.Parse(Console.ReadLine());

            Console.WriteLine(calculation1(a, b));
            Console.WriteLine(calculation2(a, b));
            Console.WriteLine(calculation3(a, b));
            Console.WriteLine(calculation4(a, b));

        }

        static string calculation1 (int a, int b)
        {
            return $"{a} + {b} = {a + b}";
        }


        static string calculation2 (int a, int b)
        {
            return $"{a} - {b} = {a - b}";
        }

        static string calculation3(int a, int b)
        {
            return $"{a} / {b} = {a / b}";
        }

        static string calculation4(int a, int b)
        {
            return $"{a} * {b} = {a * b}";
        }
    }
}
